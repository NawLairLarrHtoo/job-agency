package pos.company;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import common.DBConnection;

public class CompanyList extends JFrame {

	private JPanel contentPane;
	private JTable table_1;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_4;
	private JTextField textField_3;
	private JLabel lblNewLabel_1;

	/**
	 * Create the frame.
	 */
	public CompanyList() {
		setBackground(new Color(255, 255, 255));
		setTitle("Company List");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 989, 762);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 250));
		contentPane.setForeground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(46, 53, 814, 238);
		contentPane.add(scrollPane);

		table_1 = new JTable();
		table_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = table_1.getSelectedRow();
				String value = table_1.getModel().getValueAt(row, 0).toString();
				try {
					Connection con = DBConnection.initializeDatabase();
					String sql = "select * from company where id='" + value + "'";
					PreparedStatement stmt = con.prepareStatement(sql);
					ResultSet rs = stmt.executeQuery();
					while (rs.next()) {
						textField.setText(rs.getString("name"));
						textField_1.setText(rs.getString("email"));
						textField_2.setText(rs.getString("phone"));
						textField_3.setText(rs.getString("address"));
						textField_4.setText(rs.getString("about"));
						ImageIcon imageIcon = new ImageIcon(rs.getBytes("image"));
						lblNewLabel_1.setIcon(imageIcon);

					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		getList(scrollPane);

		JLabel lblNewLabel = new JLabel("Company List");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblNewLabel.setBounds(404, 10, 100, 23);
		contentPane.add(lblNewLabel);

		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(46, 326, 265, 238);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Name");
		lblNewLabel_2.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblNewLabel_2.setBounds(430, 326, 45, 23);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_2_1 = new JLabel("Email");
		lblNewLabel_2_1.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblNewLabel_2_1.setBounds(430, 377, 45, 23);
		contentPane.add(lblNewLabel_2_1);

		JLabel lblNewLabel_2_2 = new JLabel("Phone");
		lblNewLabel_2_2.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblNewLabel_2_2.setBounds(430, 428, 56, 26);
		contentPane.add(lblNewLabel_2_2);

		JLabel lblNewLabel_2_3 = new JLabel("Address");
		lblNewLabel_2_3.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblNewLabel_2_3.setBounds(430, 489, 66, 27);
		contentPane.add(lblNewLabel_2_3);

		JLabel lblNewLabel_2_4 = new JLabel("About");
		lblNewLabel_2_4.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblNewLabel_2_4.setBounds(430, 560, 66, 27);
		contentPane.add(lblNewLabel_2_4);

		textField = new JTextField();
		textField.setBounds(583, 318, 277, 27);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(583, 370, 277, 26);
		contentPane.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(583, 422, 277, 27);
		contentPane.add(textField_2);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(583, 549, 277, 80);
		contentPane.add(textField_4);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(583, 478, 277, 46);
		contentPane.add(textField_3);

		JButton btnNewButton = new JButton("Update");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table_1.getSelectedRow();
				String value = table_1.getModel().getValueAt(row, 0).toString();
				String n = textField.getText();
				String m = textField_1.getText();
				String p = textField_2.getText();
				String add = textField_3.getText();
				String ab = textField_4.getText();
				try {
					Connection con = DBConnection.initializeDatabase();
					String sql = "update company set name=?, email=?, phone=?, address=?, about=? where id='" + value
							+ "' ";
					PreparedStatement stmt = con.prepareStatement(sql);
					stmt.setString(1, n);
					stmt.setString(2, m);
					stmt.setString(3, p);
					stmt.setString(4, add);
					stmt.setString(5, ab);
					stmt.executeUpdate();
					JOptionPane.showMessageDialog(null, "Successfully Updated");
					textField.setText(" ");
					textField_1.setText(" ");
					textField_2.setText(" ");
					textField_4.setText(" ");
					textField_3.setText(" ");
//					ResultSet rs=stmt.executeQuery();
					getList(scrollPane);

				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}

		});

		btnNewButton.setFont(new Font("Sitka Text", Font.BOLD, 14));
		btnNewButton.setBounds(583, 665, 100, 26);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Delete");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table_1.getSelectedRow();
				String value = table_1.getModel().getValueAt(row, 0).toString();
				try {
					Connection con = DBConnection.initializeDatabase();
					String sql = "delete from company where id='" + value + "'";
					PreparedStatement stmt = con.prepareStatement(sql);
					stmt.executeUpdate();
					JOptionPane.showMessageDialog(null, "Successfully Deleted");
					textField.setText(" ");
					textField_1.setText(" ");
					textField_2.setText(" ");
					textField_4.setText(" ");
					textField_3.setText(" ");
					getList(scrollPane);

				} catch (Exception e3) {
					e3.printStackTrace();
				}
			}
		});

		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		btnNewButton_1.setFont(new Font("Sitka Text", Font.BOLD, 14));
		btnNewButton_1.setBounds(775, 665, 85, 26);
		contentPane.add(btnNewButton_1);
	}

	private void getList(JScrollPane scrollPane) {
		table_1.setFont(new Font("Sitka Text", Font.PLAIN, 13));
		table_1.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "ID", "Name", "Email", "Phone", "Address", "About" }

		));
		Connection conn = DBConnection.initializeDatabase();

		try {
			PreparedStatement stmt = conn.prepareStatement("select * from company");
			ResultSet rs = stmt.executeQuery();
			Object[] row = new Object[6];
			while (rs.next()) {

				row[0] = rs.getInt("id");
				row[1] = rs.getString("name");
				row[2] = rs.getString("email");
				row[3] = rs.getString("phone");
				row[4] = rs.getString("address");
				row[5] = rs.getString("about");
				((DefaultTableModel) table_1.getModel()).addRow(row);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		scrollPane.setViewportView(table_1);
	}
}
