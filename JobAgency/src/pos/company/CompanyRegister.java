package pos.company;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import common.DBConnection;

public class CompanyRegister extends JFrame {

	private JPanel contentPane;
	private JTextField txtname;
	private JTextField txtmail;
	private JTextField txtph;
	private JTextField txtadd;
	FileInputStream fs;
	private JTextField txtabout;

	/**
	 * Launch the application.
	 */

//	public String getName() {
//		return name;
//	}
//
//
//	public void setName(JTextField name) {
//		this.name = name;
//	}
//
//
//	public JTextField getEmail() {
//		return email;
//	}
//
//
//	public void setEmail(JTextField email) {
//		this.email = email;
//	}
//
//
//	public JTextField getPh() {
//		return ph;
//	}
//
//
//	public void setPh(JTextField ph) {
//		this.ph = ph;
//	}
//
//
//	public JTextField getAdd() {
//		return add;
//	}
//
//
//	public void setAdd(JTextField add) {
//		this.add = add;
//	}

	/**
	 * Create the frame.
	 */
	public CompanyRegister() {
		setTitle("Company Register");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 641, 549);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblEmail.setBackground(new Color(255, 182, 193));
		lblEmail.setBounds(29, 108, 60, 20);
		contentPane.add(lblEmail);

		JLabel lblPhone = new JLabel("Phone");
		lblPhone.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblPhone.setBackground(new Color(255, 182, 193));
		lblPhone.setBounds(29, 149, 60, 21);
		contentPane.add(lblPhone);

		JLabel lblAddress = new JLabel("Address");
		lblAddress.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblAddress.setBackground(new Color(255, 182, 193));
		lblAddress.setBounds(29, 203, 60, 20);
		contentPane.add(lblAddress);

		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblName.setBackground(new Color(255, 182, 193));
		lblName.setBounds(29, 66, 60, 20);
		contentPane.add(lblName);

		txtname = new JTextField();
		txtname.setBackground(Color.WHITE);
		txtname.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		txtname.setBounds(196, 66, 258, 21);
		contentPane.add(txtname);
		txtname.setColumns(10);

		txtmail = new JTextField();
		txtmail.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		txtmail.setColumns(10);
		txtmail.setBackground(Color.WHITE);
		txtmail.setBounds(196, 107, 258, 22);
		contentPane.add(txtmail);

		txtph = new JTextField();
		txtph.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		txtph.setColumns(10);
		txtph.setBackground(Color.WHITE);
		txtph.setBounds(196, 148, 258, 22);
		contentPane.add(txtph);

		txtadd = new JTextField();
		txtadd.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		txtadd.setColumns(10);
		txtadd.setBackground(Color.WHITE);
		txtadd.setBounds(196, 193, 258, 66);
		contentPane.add(txtadd);

		JLabel lblPhoto = new JLabel("Photo");
		lblPhoto.setFont(new Font("Sitka Text", Font.BOLD, 14));
		lblPhoto.setBackground(new Color(255, 182, 193));
		lblPhoto.setBounds(29, 386, 60, 20);
		contentPane.add(lblPhoto);

		JButton btnNewButton = new JButton("Browse");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("*.images", "png", "jpg");
				chooser.addChoosableFileFilter(filter);
				int result = chooser.showSaveDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					String path = chooser.getSelectedFile().getAbsolutePath();
					System.out.println(path);
					File fl = new File(path);
					try {
						fs = new FileInputStream(fl);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						System.out.println("file not found");
					}
				} else if (result == JFileChooser.CANCEL_OPTION) {
					JOptionPane.showMessageDialog(null, "Please Choose Image!");

				}

			}
		});
		btnNewButton.setHorizontalAlignment(SwingConstants.LEADING);
		btnNewButton.setForeground(new Color(0, 0, 0));
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setFont(new Font("Sitka Text", Font.BOLD, 14));
		btnNewButton.setBounds(199, 382, 85, 28);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Save");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String errMessage = "";
				String n = txtname.getText();
				if (txtname.getText() == null || txtname.getText().isEmpty()) {
					errMessage = "Name is required!";
				}
//			 String n=txtname.getText();

				String p = txtph.getText();
				if (txtph.getText() == null || txtph.getText().isEmpty()) {
					errMessage = "Phone No is required!";
				}
				String m = txtmail.getText();
				if (txtmail.getText() == null || txtmail.getText().isEmpty()) {
					errMessage = "Mail is required!";
				}
				String ads = txtadd.getText();
				if (txtadd.getText() == null || txtadd.getText().isEmpty()) {
					errMessage = "Address is required!";
				}
				String ab = txtabout.getText();
				String txtTitle = txtabout.getText();
				if (txtabout.getText() == null || txtabout.getText().isEmpty()) {
					errMessage = "About is required!";
				}
				if (errMessage.isEmpty()) {
					try {
						Connection con = DBConnection.initializeDatabase();
						String sql = "insert into company(name,email,phone,address,about,image) values(?,?,?,?,?,?)";

						PreparedStatement stmt = con.prepareStatement(sql);
						stmt.setString(1, n);
						stmt.setString(2, m);
						stmt.setString(3, p);
						stmt.setString(4, ads);
						stmt.setString(5, ab);
						stmt.setBinaryStream(6, fs);
						stmt.executeUpdate();

						JOptionPane.showMessageDialog(null, "Successfully Saved");
						txtname.setText(" ");
						txtmail.setText(" ");
						txtph.setText(" ");
						txtadd.setText(" ");
						txtabout.setText("");

					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else {
					JOptionPane.showMessageDialog(null, errMessage,"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton_1.setFont(new Font("Sitka Text", Font.BOLD, 14));
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setBounds(369, 433, 85, 30);
		contentPane.add(btnNewButton_1);

		JLabel txt = new JLabel("About");
		txt.setFont(new Font("Sitka Text", Font.BOLD, 14));
		txt.setBounds(29, 285, 60, 21);
		contentPane.add(txt);

		txtabout = new JTextField();
		txtabout.setBounds(196, 269, 258, 68);
		contentPane.add(txtabout);
		txtabout.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Company");
		lblNewLabel.setFont(new Font("Sitka Text", Font.BOLD, 15));
		lblNewLabel.setBounds(301, 10, 78, 20);
		contentPane.add(lblNewLabel);
	}
}
