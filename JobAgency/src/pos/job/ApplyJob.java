package pos.job;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import common.DBConnection;
import common.enumeration.GenderType;

public class ApplyJob extends JFrame {

	private JPanel contentPane;
	private JTextField txtExpectedSalary;
	private JTextField txtEmail;
	private JTextField txtPhone;
	private JTextField txtName;

	/**
	 * Create the frame.
	 */
	public ApplyJob(int jobId) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 544, 429);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("User Info");
		lblNewLabel.setFont(new Font("Sitka Text", Font.BOLD, 15));
		lblNewLabel.setBounds(232, 10, 70, 26);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Name");
		lblNewLabel_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(31, 56, 77, 20);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_1_1 = new JLabel("Phone");
		lblNewLabel_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1.setBounds(31, 92, 77, 20);
		contentPane.add(lblNewLabel_1_1);

		JLabel lblNewLabel_1_1_1 = new JLabel("Email");
		lblNewLabel_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1_1.setBounds(31, 131, 77, 20);
		contentPane.add(lblNewLabel_1_1_1);

		JLabel lblNewLabel_1_1_2 = new JLabel("Address");
		lblNewLabel_1_1_2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1_2.setBounds(31, 167, 77, 20);
		contentPane.add(lblNewLabel_1_1_2);

		JLabel lblNewLabel_1_1_2_1 = new JLabel("Gender");
		lblNewLabel_1_1_2_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1_2_1.setBounds(31, 239, 113, 20);
		contentPane.add(lblNewLabel_1_1_2_1);

		JComboBox txtGender = new JComboBox();
		txtGender.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		txtGender.setModel(new DefaultComboBoxModel(new String[] { "Male", "Female" }));
		txtGender.setBounds(154, 237, 213, 21);
		contentPane.add(txtGender);

		txtExpectedSalary = new JTextField();
		txtExpectedSalary.setBounds(154, 269, 213, 19);
		contentPane.add(txtExpectedSalary);
		txtExpectedSalary.setColumns(10);

		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(154, 130, 213, 19);
		contentPane.add(txtEmail);

		txtPhone = new JTextField();
		txtPhone.setColumns(10);
		txtPhone.setBounds(154, 91, 213, 19);
		contentPane.add(txtPhone);

		txtName = new JTextField();
		txtName.setColumns(10);
		txtName.setBounds(154, 55, 213, 19);
		contentPane.add(txtName);

		JTextArea txtAddress = new JTextArea();
		txtAddress.setBounds(154, 165, 213, 63);
		contentPane.add(txtAddress);

		JButton btnNewButton = new JButton("Apply");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String errMessage = "";
				String userName = txtName.getText();
				String phone = txtPhone.getText();
				String email = txtEmail.getText();
				String address = txtAddress.getText();
				int gender = txtGender.getSelectedIndex();
				int totalAllowed = 0;
				try {
					Connection conn = DBConnection.initializeDatabase();
					String sql = "select j.genderType,j.total from job j where j.status=1 and j.id='" + jobId + "'";
					PreparedStatement stmt = conn.prepareStatement(sql);
					ResultSet rs = stmt.executeQuery();
					while (rs.next()) {
						int count = rs.getInt(1);
						totalAllowed = rs.getInt(2);
						if (GenderType.getDescByCode(count) != "") {
							if (GenderType.M_F.getCode() != count && count != gender) {
								errMessage = "Our job called only " + GenderType.getDescByCode(count) + "!";
							}
						}
					}
				} catch (Exception e2) {
					e2.printStackTrace();
				}

				if ((userName == null || userName.isEmpty()) && errMessage.isEmpty()) {
					errMessage = "Name is required!";
				}
				if ((phone == null || phone.isEmpty()) && errMessage.isEmpty()) {
					errMessage = "Phone is required!";
				}
				if ((email == null || email.isEmpty()) && errMessage.isEmpty()) {
					errMessage = "Email is required!";
				}
				if ((address == null || address.isEmpty()) && errMessage.isEmpty()) {
					errMessage = "Address is required!";
				}
				Double expectedSalary = 0D;
				try {
					expectedSalary = txtExpectedSalary.getText() != null && !txtExpectedSalary.getText().isEmpty()
							? Double.valueOf(txtExpectedSalary.getText())
							: 0;
				} catch (Exception e1) {
					e1.printStackTrace();
					errMessage = "Number only accepted!";
				}
				if (errMessage.isEmpty()) {
					if (totalAllowed > 0) {
						try {
							Connection conn = DBConnection.initializeDatabase();
							String sql = "Insert into apply_job (name,phone,email,address,expectedSalary,jobId,gender,createdDateTime) values(?,?,?,?,?,?,?,now())";
							PreparedStatement stmt = conn.prepareStatement(sql);
							stmt.setString(1, userName);
							stmt.setString(2, phone);
							stmt.setString(3, email);
							stmt.setString(4, address);
							stmt.setDouble(5, expectedSalary);
							stmt.setInt(6, jobId);
							stmt.setInt(7, gender);
							stmt.executeUpdate();
							JOptionPane.showMessageDialog(null, "successfully applied!");
						} catch (Exception e1) {
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, "applied fail!","Error", JOptionPane.ERROR_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(null, "Sorry,You can't apply this job.Because this job is expired!");
					}
					dispose();
				} else {
					JOptionPane.showMessageDialog(null, errMessage,"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		btnNewButton.setBounds(31, 328, 85, 21);
		contentPane.add(btnNewButton);

		JLabel lblNewLabel_1_1_2_1_1 = new JLabel("Expected Salary");
		lblNewLabel_1_1_2_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1_2_1_1.setBounds(31, 270, 113, 20);
		contentPane.add(lblNewLabel_1_1_2_1_1);
	}
}
