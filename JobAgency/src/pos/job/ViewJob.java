package pos.job;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import common.DBConnection;
import common.enumeration.GenderType;
import common.enumeration.JobType;
import javax.swing.SwingConstants;

public class ViewJob extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public ViewJob(int jobId) {
		Integer id = 0;
		String title = "";
		String name = "";
		String phone = "";
		String email = "";
		String salary = "";
		String jobType = "";
		String gender = "";
		String totalEmp = "";
		String jobDesc = "";
		String jobReq = "";

		try {
			Connection conn = DBConnection.initializeDatabase();
			String sql = "select j.id,j.title,c.name,c.phone,c.email,j.fromSalary,j.toSalary,j.negotiable,j.timeType,j.genderType,j.total,j.jobDescription,j.jobRequirements from job j join company c on j.companyId=c.id where j.status=1 and j.id='"
					+ jobId + "'";
			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					id = rs.getInt("id");
					title = rs.getString("title");
					name = rs.getString("name");
					phone = rs.getString("phone");
					email = rs.getString("email");
					double from = rs.getDouble("fromSalary");
					double to = rs.getDouble("toSalary");
					Integer negotiable = rs.getInt("negotiable");
					if (negotiable != null && negotiable > 0) {
						salary = "Negotiable";
					} else {
						salary = from + " ~ " + to;
					}
					Integer timeType = rs.getInt("timeType");
					if (timeType != null) {
						jobType = JobType.getDescByCode(rs.getInt("timeType"));
					}
					Integer genderType = rs.getInt("genderType");
					if (genderType != null) {
						gender = GenderType.getDescByCode(rs.getInt("genderType"));
					}
					totalEmp = rs.getString("total");
					jobDesc = rs.getString("jobDescription");
					jobReq = rs.getString("jobRequirements");
				}
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}

		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 764, 525);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Job Details");
		lblNewLabel.setFont(new Font("Sitka Text", Font.BOLD, 15));
		lblNewLabel.setBounds(337, 30, 129, 31);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Title :");
		lblNewLabel_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(10, 68, 129, 22);
		contentPane.add(lblNewLabel_1);

		JLabel txtTitle = new JLabel(title);
		txtTitle.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtTitle.setBounds(149, 71, 210, 31);
		contentPane.add(txtTitle);

		JLabel lblNewLabel_1_1 = new JLabel("Salary :");
		lblNewLabel_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1.setBounds(10, 113, 129, 22);
		contentPane.add(lblNewLabel_1_1);

		JLabel txtSalary = new JLabel(salary);
		txtSalary.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtSalary.setBounds(149, 112, 210, 31);
		contentPane.add(txtSalary);

		JLabel lblNewLabel_1_2 = new JLabel("Company :");
		lblNewLabel_1_2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2.setBounds(373, 68, 80, 22);
		contentPane.add(lblNewLabel_1_2);

		JLabel txtCompanyName = new JLabel(name);
		txtCompanyName.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtCompanyName.setBounds(528, 68, 210, 31);
		contentPane.add(txtCompanyName);

		JLabel lblNewLabel_1_1_1 = new JLabel("Job Type :");
		lblNewLabel_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1_1.setBounds(371, 114, 80, 22);
		contentPane.add(lblNewLabel_1_1_1);

		JLabel txtJobType = new JLabel(jobType);
		txtJobType.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtJobType.setBounds(528, 113, 210, 31);
		contentPane.add(txtJobType);

		JLabel lblNewLabel_1_1_2 = new JLabel("Phone :");
		lblNewLabel_1_1_2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1_2.setBounds(10, 154, 129, 22);
		contentPane.add(lblNewLabel_1_1_2);

		JLabel txtPhone = new JLabel(phone);
		txtPhone.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtPhone.setBounds(149, 153, 210, 31);
		contentPane.add(txtPhone);

		JLabel lblNewLabel_1_1_1_1 = new JLabel("Email :");
		lblNewLabel_1_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1_1_1.setBounds(371, 155, 80, 22);
		contentPane.add(lblNewLabel_1_1_1_1);

		JLabel txtEmail = new JLabel(email);
		txtEmail.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtEmail.setBounds(528, 154, 210, 31);
		contentPane.add(txtEmail);

		JLabel txtJobDescriptions = new JLabel("Job Descriptions :");
		txtJobDescriptions.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtJobDescriptions.setBounds(10, 197, 143, 22);
		contentPane.add(txtJobDescriptions);

		JLabel txtDesc = new JLabel(jobDesc);
		txtDesc.setVerticalAlignment(SwingConstants.TOP);
		txtDesc.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtDesc.setBounds(151, 196, 210, 67);
		contentPane.add(txtDesc);

		JLabel lblNewLabel_1_1_1_1_1 = new JLabel("Job Requirements :");
		lblNewLabel_1_1_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1_1_1_1.setBounds(373, 198, 159, 22);
		contentPane.add(lblNewLabel_1_1_1_1_1);

		JLabel txtReq = new JLabel(jobReq);
		txtReq.setVerticalAlignment(SwingConstants.TOP);
		txtReq.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtReq.setBounds(530, 197, 210, 66);
		contentPane.add(txtReq);

		JLabel lblNewLabel_1_1_2_1 = new JLabel("Gender Type :");
		lblNewLabel_1_1_2_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1_2_1.setBounds(10, 274, 129, 22);
		contentPane.add(lblNewLabel_1_1_2_1);

		JLabel txtGender = new JLabel(gender);
		txtGender.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtGender.setBounds(149, 273, 210, 31);
		contentPane.add(txtGender);

		JLabel lblNewLabel_1_1_1_1_2 = new JLabel("Total :");
		lblNewLabel_1_1_1_1_2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1_1_1_2.setBounds(371, 274, 80, 22);
		contentPane.add(lblNewLabel_1_1_1_1_2);

		JLabel txtTotal = new JLabel(totalEmp);
		txtTotal.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		txtTotal.setBounds(528, 273, 210, 31);
		contentPane.add(txtTotal);

		JButton btnNewButton = new JButton("Yes");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (checkJob(jobId)) {
					ApplyJob applyJob = new ApplyJob(jobId);
					applyJob.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "This job is invalid!","Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton.setBounds(191, 346, 85, 21);
		contentPane.add(btnNewButton);

		JLabel lblDoYouWant = new JLabel("Do you want to apply?");
		lblDoYouWant.setForeground(Color.RED);
		lblDoYouWant.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblDoYouWant.setBounds(10, 347, 171, 22);
		contentPane.add(lblDoYouWant);
	}

	private boolean checkJob(int jobId) {
		try {
			Connection conn = DBConnection.initializeDatabase();
			String sql = "select count(j.id) from job j where j.status=1 and j.id='" + jobId + "'";
			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				int count = rs.getInt(1);
				if (count > 0) {
					return true;
				}
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		return false;
	}
}
