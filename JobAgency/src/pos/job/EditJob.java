package pos.job;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import common.DBConnection;
import common.enumeration.GenderType;
import common.enumeration.JobType;
import entity.Company;

public class EditJob extends JFrame {

	private JPanel contentPane;
	private JTextField title;
	private JTextField fromSalary;
	private JTextField toSalary;
	private JTextField total;

	/**
	 * Create the frame.
	 */
	public EditJob(int jobId) {
		int id = 0;
		String txtTitle = "";
		String name = "";
		String phone = "";
		String email = "";
		double from = 0;
		double to = 0;
		int nego = 0;
		int jobType = 0;
		int gender = 0;
		int totalEmp = 0;
		String jobDesc = "";
		String jobReq = "";
		int companyId = 0;

		try {
			Connection conn = DBConnection.initializeDatabase();
			String sql = "select j.id,j.companyId,j.title,c.name,c.phone,c.email,j.fromSalary,j.toSalary,j.negotiable,j.timeType,j.genderType,j.total,j.jobDescription,j.jobRequirements from job j join company c on j.companyId=c.id where j.status=1 and j.id='"
					+ jobId + "'";
			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					id = rs.getInt("id");
					txtTitle = rs.getString("title");
					name = rs.getString("name");
					phone = rs.getString("phone");
					email = rs.getString("email");
					from = rs.getDouble("fromSalary");
					to = rs.getDouble("toSalary");
					nego = rs.getInt("negotiable");
					jobType = rs.getInt("timeType");
					gender = rs.getInt("genderType");
					totalEmp = rs.getInt("total");
					jobDesc = rs.getString("jobDescription");
					jobReq = rs.getString("jobRequirements");
					companyId = rs.getInt("companyId");
				}
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}

		setTitle("Edit Job");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 608, 592);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Job");
		lblNewLabel.setFont(new Font("Sitka Text", Font.BOLD, 15));
		lblNewLabel.setBounds(303, 21, 33, 22);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Title");
		lblNewLabel_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(62, 65, 45, 22);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_1_1 = new JLabel("From Salary");
		lblNewLabel_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1.setBounds(62, 97, 87, 22);
		contentPane.add(lblNewLabel_1_1);

		JLabel lblNewLabel_1_2 = new JLabel("To Salary");
		lblNewLabel_1_2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2.setBounds(62, 125, 87, 22);
		contentPane.add(lblNewLabel_1_2);

		JLabel lblNewLabel_1_2_1_1 = new JLabel("Job Type");
		lblNewLabel_1_2_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1.setBounds(62, 157, 87, 22);
		contentPane.add(lblNewLabel_1_2_1_1);

		JLabel lblNewLabel_1_2_1_1_1 = new JLabel("Job Description");
		lblNewLabel_1_2_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1_1.setBounds(62, 189, 117, 22);
		contentPane.add(lblNewLabel_1_2_1_1_1);

		JLabel lblNewLabel_1_2_1_1_1_1 = new JLabel("Job Requirements");
		lblNewLabel_1_2_1_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1_1_1.setBounds(62, 261, 127, 22);
		contentPane.add(lblNewLabel_1_2_1_1_1_1);

		JLabel lblNewLabel_1_2_1_1_1_1_1 = new JLabel("Gender");
		lblNewLabel_1_2_1_1_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1_1_1_1.setBounds(62, 345, 63, 22);
		contentPane.add(lblNewLabel_1_2_1_1_1_1_1);

		JLabel lblNewLabel_1_2_1_1_1_1_1_1 = new JLabel("Total");
		lblNewLabel_1_2_1_1_1_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1_1_1_1_1.setBounds(62, 375, 45, 22);
		contentPane.add(lblNewLabel_1_2_1_1_1_1_1_1);

		title = new JTextField(txtTitle);
		title.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		title.setBounds(195, 65, 203, 19);
		contentPane.add(title);
		title.setColumns(10);

		fromSalary = new JTextField(String.valueOf(from));
		fromSalary.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		fromSalary.setColumns(10);
		fromSalary.setBounds(195, 97, 203, 19);
		contentPane.add(fromSalary);

		toSalary = new JTextField(String.valueOf(to));
		toSalary.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		toSalary.setColumns(10);
		toSalary.setBounds(195, 125, 203, 19);
		contentPane.add(toSalary);

		JCheckBox negotiable = new JCheckBox("Negotiate");
		negotiable.setSelected(nego > 0 ? true : false);
		negotiable.setBounds(410, 124, 93, 21);
		contentPane.add(negotiable);

		JTextArea jobDescription = new JTextArea(jobDesc);
		jobDescription.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		jobDescription.setBounds(195, 189, 200, 64);
		contentPane.add(jobDescription);

		JTextArea jobRequirements = new JTextArea(jobReq);
		jobRequirements.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		jobRequirements.setRows(5);
		jobRequirements.setBounds(195, 258, 200, 64);
		contentPane.add(jobRequirements);

		JComboBox genderType = new JComboBox();
		genderType.setModel(new DefaultComboBoxModel(new String[] { "Male", "Female", "M/F" }));
		genderType.setSelectedIndex(gender);
		genderType.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		genderType.setBounds(195, 343, 203, 22);
		contentPane.add(genderType);

		total = new JTextField(String.valueOf(totalEmp));
		total.setColumns(10);
		total.setBounds(195, 375, 203, 19);
		contentPane.add(total);

		JComboBox timeType = new JComboBox();
		timeType.setModel(new DefaultComboBoxModel(new String[] { "Full Time", "Part Time" }));
		timeType.setSelectedIndex(jobType);
		timeType.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		timeType.setBounds(195, 156, 203, 22);
		contentPane.add(timeType);

		List<Company> companyObjList = new ArrayList<Company>();
		JComboBox companyList = new JComboBox();
		companyList.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		companyList.setBounds(195, 409, 203, 22);
		try {
			Connection conn = DBConnection.initializeDatabase();
			PreparedStatement stmt = conn.prepareStatement("select id,name from company");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				int compId = rs.getInt("id");
				String compName = rs.getString("name").trim();
				Company comboItem = new Company(compId, compName);
				companyList.addItem(compName);
				companyObjList.add(comboItem);
				if (companyId == compId) {
					companyList.setSelectedItem(compName);
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		contentPane.add(companyList);

		JButton saveBtn = new JButton("Update");
		saveBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String errMessage = "";
				String txtTitle = title.getText();
				if (title.getText() == null || title.getText().isEmpty()) {
					errMessage = "Title is required!";
				}
				int txtNegotiable = negotiable.isSelected() ? 1 : 0;
				if (!negotiable.isSelected() && errMessage.isEmpty()) {
					if (fromSalary.getText() == null || fromSalary.getText().isEmpty()) {
						errMessage = "From Salary is required!";
					} else if (toSalary.getText() == null || toSalary.getText().isEmpty()) {
						errMessage = "To Salary is required!";
					}
				}
				double txtFromSalary = fromSalary.getText() != null && !fromSalary.getText().isEmpty()
						? Double.parseDouble(fromSalary.getText())
						: 0;
				double txtToSalary = toSalary.getText() != null && !toSalary.getText().isEmpty()
						? Double.parseDouble(toSalary.getText())
						: 0;
				int txtTimeType = timeType.getSelectedIndex();
				String txtJobDescriptions = jobDescription.getText();
				String txtJobRequirements = jobRequirements.getText();
				int txtGender = genderType.getSelectedIndex();
				int txtTotal=0;
				try {
					txtTotal = total.getText() != null && !total.getText().isEmpty()
							? Integer.valueOf(total.getText())
							: 0;
					if (txtTotal < 1 && errMessage.isEmpty()) {
						errMessage = "Total is required!";
					}
				} catch (Exception tErr) {
					tErr.printStackTrace();
					errMessage = "Number only accepted!";
				}
				if (companyList.getSelectedIndex() < 0 && errMessage.isEmpty()) {
					errMessage = "Please select one company!";
				}
				int txtCompanyId = companyList.getSelectedIndex();
				if (errMessage.isEmpty() && companyObjList != null && companyObjList.size() > 0) {
					try {
						Connection conn = DBConnection.initializeDatabase();
						String sql = "Update job set title=?,fromSalary=?,toSalary=?,negotiable=?,timeType=?,jobDescription=?,jobRequirements=?,genderType=?,total=?,companyId=? where id=?";
						PreparedStatement stmt = conn.prepareStatement(sql);
						stmt.setString(1, txtTitle);
						stmt.setDouble(2, txtFromSalary);
						stmt.setDouble(3, txtToSalary);
						stmt.setInt(4, txtNegotiable);
						stmt.setInt(5, txtTimeType);
						stmt.setString(6, txtJobDescriptions);
						stmt.setString(7, txtJobRequirements);
						stmt.setInt(8, txtGender);
						stmt.setInt(9, txtTotal);
						stmt.setInt(10, companyObjList.get(txtCompanyId).getId());
						stmt.setInt(11, jobId);
						stmt.executeUpdate();
						JOptionPane.showMessageDialog(null, "successfully updated!");
					} catch (Exception e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null, "updated fail!","Error", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, errMessage,"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		saveBtn.setBounds(313, 441, 85, 21);
		contentPane.add(saveBtn);

		JLabel lblNewLabel_1_2_1_1_1_1_1_1_1 = new JLabel("Company");
		lblNewLabel_1_2_1_1_1_1_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1_1_1_1_1_1.setBounds(62, 409, 87, 22);
		contentPane.add(lblNewLabel_1_2_1_1_1_1_1_1_1);
	}

}
