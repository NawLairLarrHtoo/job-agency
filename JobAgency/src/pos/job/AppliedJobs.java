package pos.job;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import common.DBConnection;
import common.enumeration.AcceptedType;
import common.enumeration.GenderType;

public class AppliedJobs extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton acceptBtn;
	String jobId = "";
	String applyJobId = "";
	String typeDesc = "";

	/**
	 * Create the frame.
	 */
	public AppliedJobs() {
		setTitle("Applied Jobs");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 965, 408);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Applied Jobs");
		lblNewLabel.setFont(new Font("Sitka Text", Font.BOLD, 15));
		lblNewLabel.setBounds(314, 10, 105, 28);
		contentPane.add(lblNewLabel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 52, 931, 248);
		contentPane.add(scrollPane);

		getLists(scrollPane);
	}

	private void getLists(JScrollPane scrollPane) {
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = table.getSelectedRow();
				applyJobId = table.getModel().getValueAt(row, 0).toString();
				jobId = table.getModel().getValueAt(row, 1).toString();
				typeDesc = table.getModel().getValueAt(row, 10).toString();

				if (typeDesc == null || typeDesc.isEmpty() || typeDesc.equals(AcceptedType.REJECT.getDesc())) {
					acceptBtn = new JButton("Accept");
					acceptBtn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							String errorMsg = "";
							if (jobId == null || jobId == "") {
								errorMsg = "Please select any row!";
							}
							if (errorMsg.isEmpty()) {
								try {
									int totalAllowed = 0;
									Connection conn = DBConnection.initializeDatabase();
									String sql = "select j.genderType,j.total from job j where j.status=1 and j.id='"
											+ jobId + "'";
									PreparedStatement stmt = conn.prepareStatement(sql);
									ResultSet rs = stmt.executeQuery();
									while (rs.next()) {
										int count = rs.getInt(1);
										totalAllowed = rs.getInt(2);
									}

									if (totalAllowed > 0) {
										// Update Job
										StringBuilder sql2 = new StringBuilder();
										sql2.append("update job set total=" + (totalAllowed - 1));
										if (totalAllowed == 1) {
											sql2.append(",status=false");
										}
										sql2.append(" where id=" + jobId);
										PreparedStatement stmt2 = conn.prepareStatement(sql2.toString());
										stmt2.executeUpdate();

										// Update Apply Job
										StringBuilder sql3 = new StringBuilder();
										sql3.append(
												"update apply_job set acceptedType=" + AcceptedType.ACCEPT.getCode());
										sql3.append(" where id=" + applyJobId);
										PreparedStatement stmt3 = conn.prepareStatement(sql3.toString());
										stmt3.executeUpdate();

										if (totalAllowed == 1) {
											StringBuilder sql4 = new StringBuilder();
											sql4.append("update apply_job set acceptedType="
													+ AcceptedType.REJECT.getCode());
											sql4.append(" where jobId=" + jobId + " and acceptedType is null");
											PreparedStatement stmt4 = conn.prepareStatement(sql4.toString());
											stmt4.executeUpdate();
										}

										JOptionPane.showMessageDialog(null, "successfully updated!");
										getLists(scrollPane);
									} else {
										JOptionPane.showMessageDialog(null,
												"Sorry,You can't apply this job.Because this job is expired!");
									}
								} catch (Exception e1) {
									e1.printStackTrace();
									JOptionPane.showMessageDialog(null, "updated fail!","Error", JOptionPane.ERROR_MESSAGE);
								}
							}
						}
					});
					acceptBtn.setBounds(20, 306, 85, 21);
					contentPane.add(acceptBtn);
					contentPane.revalidate();
					contentPane.repaint();
				} else {
					if (acceptBtn != null) {
						contentPane.remove(acceptBtn);
						contentPane.revalidate();
						contentPane.repaint();
					}
				}
			}
		});
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Id", "JobId", "Title", "Company",
				"User", "User Email", "User Phone", "User Address", "Expected Salary", "Gender", "Status" }) {
			boolean[] columnEditables = new boolean[] { false, false, false, false, false, false, false, false, false,
					false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		try {
			Connection con = DBConnection.initializeDatabase();
			PreparedStatement stmt = con.prepareStatement(
					"select aj.id,j.id,j.title,c.name,aj.name,aj.phone,aj.email,aj.address,aj.expectedSalary,aj.gender,aj.acceptedType from apply_job aj join job j on aj.jobId=j.id join company c on j.companyId=c.id order by createdDateTime desc");
			ResultSet rs = stmt.executeQuery();
			Object[] row = new Object[11];
			while (rs.next()) {
				row[0] = rs.getInt(1);
				row[1] = rs.getInt(2);
				row[2] = rs.getString(3);
				row[3] = rs.getString(4);
				row[4] = rs.getString(5);
				row[5] = rs.getString(6);
				row[6] = rs.getString(7);
				row[7] = rs.getString(8);
				row[8] = rs.getDouble(9);
				row[9] = rs.getInt(10) > -1 ? GenderType.getDescByCode(rs.getInt(10)) : "";
				row[10] = rs.getString(11) != null && rs.getString(11) != "" ? AcceptedType.getDescByCode(rs.getInt(11))
						: "";
				((DefaultTableModel) table.getModel()).addRow(row);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		scrollPane.setViewportView(table);
	}
}
