package pos.job;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import common.DBConnection;
import common.enumeration.GenderType;
import common.enumeration.JobType;

public class JobList extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private String jobId;

	/**
	 * Create the frame.
	 */
	public JobList() {
		setTitle("Upcoming Jobs");
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 975, 485);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 46, 941, 313);
		contentPane.add(scrollPane);

		getJobLists(scrollPane);

		JLabel lblNewLabel = new JLabel("Job List");
		lblNewLabel.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel.setBounds(459, 10, 54, 26);
		contentPane.add(lblNewLabel);

		JButton btnNewButton = new JButton("View");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jobId != null && jobId != "") {
					if (checkJob(jobId)) {
						ViewJob viewJob = new ViewJob(Integer.valueOf(jobId));
						viewJob.setVisible(true);
					} else {
						JOptionPane.showMessageDialog(null, "Please select any row!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Please select any row!", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton.setBounds(105, 369, 85, 21);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Edit");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jobId != null && jobId != "") {
					if (checkJob(jobId)) {
						EditJob editJob = new EditJob(Integer.valueOf(jobId));
						editJob.setVisible(true);
					} else {
						JOptionPane.showMessageDialog(null, "Please select any row!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Please select any row!", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton_1.setBounds(201, 369, 85, 21);
		contentPane.add(btnNewButton_1);

		JButton btnNewButton_1_1 = new JButton("Delete");
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jobId != null && jobId != "") {
					try {
						if (checkJob(jobId)) {
							Connection con = DBConnection.initializeDatabase();
							PreparedStatement stmt = con.prepareStatement("delete from job where id=" + jobId + "");
							stmt.executeUpdate();
							JOptionPane.showMessageDialog(null, "Successfully Deleted!");
							getJobLists(scrollPane);
						} else {
							JOptionPane.showMessageDialog(null, "Please select any row!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					} catch (Exception e2) {
						e2.printStackTrace();
						JOptionPane.showMessageDialog(null, "Delete failed!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Please select any row!", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton_1_1.setBounds(296, 369, 85, 21);
		contentPane.add(btnNewButton_1_1);

		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getJobLists(scrollPane);
			}
		});
		btnRefresh.setBounds(10, 369, 85, 21);
		contentPane.add(btnRefresh);
	}

	private void getJobLists(JScrollPane scrollPane) {
		table = new JTable();
		table.setSurrendersFocusOnKeystroke(true);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = table.getSelectedRow();
				jobId = table.getModel().getValueAt(row, 0).toString();
			}
		});
		table.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Id", "Title", "Company", "Phone", "Email", "Salary", "Job Type", "Gender", "Total" }){
			boolean[] columnEditables = new boolean[] { false, false, false, false, false, false, false, false, false};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		try {
			Connection con = DBConnection.initializeDatabase();
			PreparedStatement stmt = con.prepareStatement(
					"select j.id,j.title,c.name,c.phone,c.email,j.fromSalary,j.toSalary,j.negotiable,j.timeType,j.genderType,j.total from job j join company c on j.companyId=c.id where j.status=1 order by title");
			ResultSet rs = stmt.executeQuery();
			Object[] row = new Object[10];
			while (rs.next()) {
				row[0] = rs.getInt("id");
				row[1] = rs.getString("title");
				row[2] = rs.getString("name");
				row[3] = rs.getString("phone");
				row[4] = rs.getString("email");
				double from = rs.getDouble("fromSalary");
				double to = rs.getDouble("toSalary");
				Integer negotiable = rs.getInt("negotiable");
				if (negotiable != null && negotiable > 0) {
					row[5] = "Negotiable";
				} else {
					row[5] = from + " ~ " + to;
				}
				Integer timeType = rs.getInt("timeType");
				if (timeType != null) {
					row[6] = JobType.getDescByCode(rs.getInt("timeType"));
				}
				Integer genderType = rs.getInt("genderType");
				if (genderType != null) {
					row[7] = GenderType.getDescByCode(rs.getInt("genderType"));
				}
				row[8] = rs.getInt("total");
				((DefaultTableModel) table.getModel()).addRow(row);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		scrollPane.setViewportView(table);
	}

	private boolean checkJob(String jobId) {
		try {
			Connection conn = DBConnection.initializeDatabase();
			String sql = "select count(j.id) from job j where j.status=1 and j.id='" + jobId + "'";
			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				int count = rs.getInt(1);
				if (count > 0) {
					return true;
				}
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		return false;
	}
}
