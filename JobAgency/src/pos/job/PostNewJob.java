package pos.job;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import common.DBConnection;
import entity.Company;

public class PostNewJob extends JFrame {

	private JPanel contentPane;
	private JTextField title;
	private JTextField fromSalary;
	private JTextField toSalary;
	private JTextField total;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public PostNewJob() {
		setTitle("Post New Job");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 608, 592);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Job");
		lblNewLabel.setFont(new Font("Sitka Text", Font.BOLD, 15));
		lblNewLabel.setBounds(303, 21, 33, 22);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Title");
		lblNewLabel_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(62, 65, 45, 22);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_1_1 = new JLabel("From Salary");
		lblNewLabel_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_1.setBounds(62, 97, 87, 22);
		contentPane.add(lblNewLabel_1_1);

		JLabel lblNewLabel_1_2 = new JLabel("To Salary");
		lblNewLabel_1_2.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2.setBounds(62, 125, 87, 22);
		contentPane.add(lblNewLabel_1_2);

		JLabel lblNewLabel_1_2_1_1 = new JLabel("Job Type");
		lblNewLabel_1_2_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1.setBounds(62, 157, 87, 22);
		contentPane.add(lblNewLabel_1_2_1_1);

		JLabel lblNewLabel_1_2_1_1_1 = new JLabel("Job Description");
		lblNewLabel_1_2_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1_1.setBounds(62, 189, 117, 22);
		contentPane.add(lblNewLabel_1_2_1_1_1);

		JLabel lblNewLabel_1_2_1_1_1_1 = new JLabel("Job Requirements");
		lblNewLabel_1_2_1_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1_1_1.setBounds(62, 261, 127, 22);
		contentPane.add(lblNewLabel_1_2_1_1_1_1);

		JLabel lblNewLabel_1_2_1_1_1_1_1 = new JLabel("Gender");
		lblNewLabel_1_2_1_1_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1_1_1_1.setBounds(62, 345, 63, 22);
		contentPane.add(lblNewLabel_1_2_1_1_1_1_1);

		JLabel lblNewLabel_1_2_1_1_1_1_1_1 = new JLabel("Total");
		lblNewLabel_1_2_1_1_1_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1_1_1_1_1.setBounds(62, 375, 45, 22);
		contentPane.add(lblNewLabel_1_2_1_1_1_1_1_1);

		title = new JTextField();
		title.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		title.setBounds(195, 65, 203, 19);
		contentPane.add(title);
		title.setColumns(10);

		fromSalary = new JTextField();
		fromSalary.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		fromSalary.setColumns(10);
		fromSalary.setBounds(195, 97, 203, 19);
		contentPane.add(fromSalary);

		toSalary = new JTextField();
		toSalary.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		toSalary.setColumns(10);
		toSalary.setBounds(195, 125, 203, 19);
		contentPane.add(toSalary);

		JCheckBox negotiable = new JCheckBox("Negotiate");
		negotiable.setBounds(410, 124, 93, 21);
		contentPane.add(negotiable);

		JTextArea jobDescription = new JTextArea();
		jobDescription.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		jobDescription.setBounds(195, 189, 200, 64);
		contentPane.add(jobDescription);

		JTextArea jobRequirements = new JTextArea();
		jobRequirements.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		jobRequirements.setRows(5);
		jobRequirements.setBounds(195, 258, 200, 64);
		contentPane.add(jobRequirements);

		JComboBox genderType = new JComboBox();
		genderType.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		genderType.setModel(new DefaultComboBoxModel(new String[] { "Male", "Female", "M/F" }));
		genderType.setBounds(195, 343, 203, 22);
		contentPane.add(genderType);

		total = new JTextField();
		total.setColumns(10);
		total.setBounds(195, 375, 203, 19);
		contentPane.add(total);

		JComboBox timeType = new JComboBox();
		timeType.setModel(new DefaultComboBoxModel(new String[] { "Full Time", "Part Time" }));
		timeType.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		timeType.setBounds(195, 156, 203, 22);
		contentPane.add(timeType);

		List<Company> companyObjList = new ArrayList<Company>();
		JComboBox companyList = new JComboBox();
		companyList.setFont(new Font("Sitka Text", Font.PLAIN, 10));
		companyList.setBounds(195, 409, 203, 22);
		try {
			Connection conn = DBConnection.initializeDatabase();
			PreparedStatement stmt = conn.prepareStatement("select id,name from company");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name").trim();
				Company comboItem = new Company(id, name);
				companyList.addItem(name);
				companyObjList.add(comboItem);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		contentPane.add(companyList);

		JButton saveBtn = new JButton("Save");
		saveBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String errMessage = "";
				String txtTitle = title.getText();
				if (title.getText() == null || title.getText().isEmpty()) {
					errMessage = "Title is required!";
				}
				int txtNegotiable = negotiable.isSelected() ? 1 : 0;
				if (!negotiable.isSelected() && errMessage.isEmpty()) {
					if (fromSalary.getText() == null || fromSalary.getText().isEmpty()) {
						errMessage = "From Salary is required!";
					} else if (toSalary.getText() == null || toSalary.getText().isEmpty()) {
						errMessage = "To Salary is required!";
					}
				}
				double txtFromSalary = fromSalary.getText() != null && !fromSalary.getText().isEmpty()
						? Double.parseDouble(fromSalary.getText())
						: 0;
				double txtToSalary = toSalary.getText() != null && !toSalary.getText().isEmpty()
						? Double.parseDouble(toSalary.getText())
						: 0;
				int txtTimeType = timeType.getSelectedIndex();
				String txtJobDescriptions = jobDescription.getText();
				String txtJobRequirements = jobRequirements.getText();
				int txtGender = genderType.getSelectedIndex();
				int txtTotal=0;
				try {
					txtTotal = total.getText() != null && !total.getText().isEmpty()
							? Integer.valueOf(total.getText())
							: 0;
					if (txtTotal < 1 && errMessage.isEmpty()) {
						errMessage = "Total is required!";
					}
				} catch (Exception tErr) {
					tErr.printStackTrace();
					errMessage = "Number only accepted!";
				}
				if (companyList.getSelectedIndex() < 0 && errMessage.isEmpty()) {
					errMessage = "Please select one company!";
				}
				int txtCompanyId = companyList.getSelectedIndex();
				if (errMessage.isEmpty() && companyObjList != null && companyObjList.size() > 0) {
					try {
						Connection conn = DBConnection.initializeDatabase();
						String sql = "Insert into job (title,fromSalary,toSalary,negotiable,timeType,jobDescription,jobRequirements,genderType,total,status,companyId) values(?,?,?,?,?,?,?,?,?,?,?)";
						PreparedStatement stmt = conn.prepareStatement(sql);
						stmt.setString(1, txtTitle);
						stmt.setDouble(2, txtFromSalary);
						stmt.setDouble(3, txtToSalary);
						stmt.setInt(4, txtNegotiable);
						stmt.setInt(5, txtTimeType);
						stmt.setString(6, txtJobDescriptions);
						stmt.setString(7, txtJobRequirements);
						stmt.setInt(8, txtGender);
						stmt.setInt(9, txtTotal);
						stmt.setInt(10, 1);
						stmt.setInt(11, companyObjList.get(txtCompanyId).getId());
						stmt.executeUpdate();
						JOptionPane.showMessageDialog(null, "successfully saved!");

						title.setText("");
						fromSalary.setText("");
						toSalary.setText("");
						negotiable.setSelected(false);
						timeType.setSelectedIndex(0);
						jobDescription.setText("");
						jobRequirements.setText("");
						genderType.setSelectedIndex(0);
						total.setText("");
						companyList.setSelectedIndex(-1);
					} catch (Exception e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null, "saved fail!","Error", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, errMessage,"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		saveBtn.setBounds(313, 441, 85, 21);
		contentPane.add(saveBtn);

		JLabel lblNewLabel_1_2_1_1_1_1_1_1_1 = new JLabel("Company");
		lblNewLabel_1_2_1_1_1_1_1_1_1.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		lblNewLabel_1_2_1_1_1_1_1_1_1.setBounds(62, 409, 87, 22);
		contentPane.add(lblNewLabel_1_2_1_1_1_1_1_1_1);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
