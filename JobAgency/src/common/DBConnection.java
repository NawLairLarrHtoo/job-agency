package common;
import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {

 public static Connection initializeDatabase(){
	 Connection con=null;
	try
	{
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/job_agency", "root", "root");
		System.out.println("Connect Successfully");
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	return con;
	
}
}
