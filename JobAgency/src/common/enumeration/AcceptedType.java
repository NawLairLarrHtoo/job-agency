package common.enumeration;

public enum AcceptedType {
	ACCEPT(0, "Accept"), REJECT(1, "Reject");

	private int code;
	private String desc;

	AcceptedType(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public static String getDescByCode(int code) {
		for (AcceptedType us : values()) {
			if (us.getCode() == code) {
				return us.getDesc();
			}
		}
		return "";

	}

}