package common.enumeration;

public enum GenderType {
	MALE(0, "Male"), FEMALE(1, "Female"),M_F(2,"M/F");

	private int code;
	private String desc;

	GenderType(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public static String getDescByCode(int code) {
		for (GenderType us : values()) {
			if (us.getCode() == code) {
				return us.getDesc();
			}
		}
		return "";

	}

}