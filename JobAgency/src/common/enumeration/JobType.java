package common.enumeration;

public enum JobType {
	FULLTIME(0, "Full Time"), PARTTIME(1, "Part Time");

	private int code;
	private String desc;

	JobType(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public static String getDescByCode(int code) {
		for (JobType us : values()) {
			if (us.getCode() == code) {
				return us.getDesc();
			}
		}
		return "";

	}

}