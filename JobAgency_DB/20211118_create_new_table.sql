
CREATE TABLE `job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `fromSalary` double default null,
  `toSalary` double default null,
  `negotiable` int default null,
  `timeType` int default null,
  `jobDescription` varchar(1000) default null,
  `jobRequirements` varchar(1000) default null,
  `companyId` int(11) default null,
  `genderType` int(1) default null,
  `total` int(2) default null,
  `status` int(2) default null,
  PRIMARY KEY (`id`),
   CONSTRAINT `company_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `company` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `apply_job`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) DEFAULT NULL,
    `phone` varchar(50) DEFAULT NULL,
    `email` varchar(50) DEFAULT NULL,
    `image` blob,
    `jobId` int(11) default null,
    `acceptantType` int(1) default null,
    Primary key(id),
    CONSTRAINT `job_ibfk_1` FOREIGN KEY (`jobId`) REFERENCES `job` (`Id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;